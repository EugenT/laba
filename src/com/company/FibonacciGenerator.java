package com.company;


public final class FibonacciGenerator {

    public static int[] getFibonacciSequance(int num) {
        int[] arr = new int[num];

        int first = 0;
        int second = 1;
        int next;

        arr[0] = first;
        arr[1] = second;

        for(int i = 2; i < num; i++) {
            next = first + second;
            first = second;
            second = next;
            arr[i] = next;
        }
        return arr;
    }
}
