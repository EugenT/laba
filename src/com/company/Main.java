package com.company;

import javax.lang.model.type.ArrayType;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        ArrayList<Human> humanList = new ArrayList<Human>();

        humanList.add(0, new Human("Stive","Thyganok",
                new GregorianCalendar(1998, 5, 19),
                new Address("street", 12)));
        humanList.add(1, new Human("Nancy","Somesurname",
                new GregorianCalendar(1988, 7, 11),
                new Address("GrowStreet", 12)));

        humanList.add(2, new Human("Jack","Jack",
                new GregorianCalendar(1977, 1, 4),
                new Address("street", 12)));

        humanList.add(3, new Human("Name1","Surname",
                new GregorianCalendar(2007, 1, 4),
                new Address("street", 12)));

        humanList.add(4, new Human("Name2","Surname2",
                new GregorianCalendar(1966, 1, 4),
                new Address("street", 12)));

        humanList.add(5, new Human("Name3","Surname",
                new GregorianCalendar(2003, 1, 4),
                new Address("street", 12)));

        humanList.add(6, new Human("Name4","Surname",
                new GregorianCalendar(2008, 1, 4),
                new Address("GrowStreet", 12)));

        Collections.sort(humanList);    //Sort array of human object to use certain methods
        //Search by name
        searchByName(humanList, "Jack");
        //Search by address attrib
        searchByAddressAttrib(humanList, new Address("GrowStreet"));
        //Search people by date in a specific range
        searchInDateRange(humanList, new GregorianCalendar(2000,1,1),
                new GregorianCalendar(2010, 1,1));
        //Seatch the oledest(youngest) one
        System.out.println(searchByAge(humanList, "OLDEST").toString());
        //Search for people living on the same street
        System.out.println( " ");
        searchPeopleOnTheSameStreet(humanList, new Address("GrowStreet"));


        int[] fib = FibonacciGenerator.getFibonacciSequance(15);
        for(int n : fib) {
            System.out.print(n + " ");
        }
    }

    private static void searchByStreetName(ArrayList<Human> list, Address address) {
        for(Human h : list) {
            if((h.getAddress().getStreet()).equals(address.getStreet())) {
                System.out.println(h.toString() + "\n");
            }
        }
    }


    private static boolean searchByName(ArrayList<Human> list, String name) {
        for(Human h : list) {
            if(h.getName().equals(name)) {
                System.out.print(h.toString() + "\n");
                return true;
            }
        }
        return false;
    }

    private static Human searchByAge(ArrayList<Human> list, String flag) {
        if(flag.equals("OLDEST")) {
            return list.get(0);
        }
        if(flag.equals("YOUNGEST")) {
            return list.get(list.size() - 1);
        }
        return new Human();
    }


    private static void searchPeopleOnTheSameStreet(ArrayList<Human> list, Address address) {
        for(Human h : list) {
            if(h.getAddress().getStreet().equals(address.getStreet())) {
                System.out.println(h.toString());
            }
        }
    }

    private static boolean searchByAddressAttrib(ArrayList<Human> list, Address address) {
        if(address.getStreet() != null) {
            for(Human h : list) {
                if(h.getAddress().getStreet().equals(address.getStreet())) {
                    System.out.println(h.toString());
                    return true;
                }
            }
        }
        else {
            for(Human h : list) {
                if(h.getAddress().getStreet().equals(address.getHouseId())) {
                    System.out.println(h.toString());
                    return true;
                }
            }
        }
        return false;
    }

    private static void searchInDateRange(ArrayList<Human> list, Calendar leftRange, Calendar rightRange) {
        for(Human h : list) {
            if(h.getBirthDate().getTimeInMillis() > leftRange.getTimeInMillis() &&
                    h.getBirthDate().getTimeInMillis() < rightRange.getTimeInMillis()) {
                System.out.println(h.toString());
            }
        }
    }
}

